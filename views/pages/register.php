<html>

<head>

<link rel="stylesheet" type="text/css" href="styles/style_register.css">
<title>Register</title>
</head>

<body>

<div class="registerArea">
<form action="?controller=posts&action=register" method="POST">
<input type="text" class="inputBox" name="username" placeholder="Username" value="<?php if(isset($_GET['username'])){ echo $_GET['username']; } ?>">
<br><input type="password" class="inputBox" name="password" placeholder="Password">
<br><input type="password" class="inputBox" name="confpassword" placeholder="Confirm password">
<br><input type="text" class="inputBox" name="email" placeholder="Email" value="<?php if(isset($_GET['email'])) { echo $_GET['email']; } ?>">
<br><input type="submit" class="btnRegister" value="Register">
</form>
</div>

<div class="errorArea">
<?php 
if(isset($_GET['err']))
{
	switch($_GET['err'])
	{
		case 1: echo "Password and confirm password mismatch."; break;
		case 2: echo "Username already existed."; break;
		case 3: echo "Email already registered."; break;
	}
}
?>
</div>

</body>

</html> 