<html>
<head>
<link rel="stylesheet" type="text/css" href="styles/style_home.css">
<title>Home</title>
</head>

<body>
<?php

if(Session::check_login())
{
	echo "<div class='head'>Welcome, " . $_SESSION['username'] . "!</div>";
	echo "<div class='subhead'>If you're new here, you can start <a href='?controller=posts&action=edit&id=new'>creating your article</a> here</div>";
	echo "<div class='subhead'>or <a href='?controller=posts&action=index'>visit</a> someone else's articles.</div>";
}
else
{
	echo "<div class='head'>Welcome to Simple Blog!</div>";
	echo "<div class='subhead'>To get full access to every articles and functions, please register or sign in.</div>";
}

?>
</body>

</html>