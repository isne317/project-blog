<html>

<head>
<title>Preview</title>
<link rel="stylesheet" type="text/css" href="styles/style_Preview.css">

</head>

<?php

$topic = str_replace(array('"',"'","<",">","[i]","[/i]"),array("&#34","&#39","&#60","&#62","<i>","</i>"),$_POST['topic']);
$content = str_replace(array('"',"'","<",">",'[link=&#34','&#34/link]',"[/link]",'[img=&#34','&#34/img]',"[i]","[/i]","[b]","[/b]"),array("&#34","&#39","&#60","&#62","<a href='","'ca>","</a>","<img src='","'cimg>","<i>","</i>","<b>","</b>"),$_POST['content']);
$member = $_POST['member'];
$comment = $_POST['comment'];
$author_id = $_SESSION['id'];

?>

<body>

<div class="heading">
Preview Section
</div>
<div class="subheading">
This is what your audiences will see. Please make sure every indent/spacing is correctly placed before pressing Publish.
Press back if you want to edit.
<hr>
</div>

<div class="area1">
<div class="topic">

<?php 

echo $topic;

?>
</div>

<div class="content">

<?php

echo nl2br($content);

?>

</div>
</div>

<div class="area2">
<div class="clear"></div>
<div class="subheading">
<hr>
</div>

<div class="XRinfo">Post in category: <?php echo $cat->name; ?> <br>
<?php

if($member == 1)
{
	echo "Only registered audiences can see this article.";
}
else
{
	echo "Everyone can see this article.";
}
echo "<br>";
if($comment == 1)
{
	echo "Registered audiences can comment on this article.";
}
else
{
	echo "Comment has beed disabled.";
}

?>
</div>

<form action="?controller=posts&action=process&id=<?php echo $_GET['id']; ?>" method="POST">
<input type="submit" class="btnPublish" value="Publish">
<input type="hidden" name="tpc" value = "<?php echo $topic; ?>">
<input type="hidden" name="cnt" value = "<?php echo $content; ?>">
<input type="hidden" name="cat" value = <?php echo $cat->id; ?>>
<input type="hidden" name="mem" value = <?php echo $member; ?>>
<input type="hidden" name="com" value = <?php echo $comment; ?>>
<input type="hidden" name="au" value = <?php echo $author_id; ?>>

</form>

</div>
</body>

</html>