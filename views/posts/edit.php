<html>

<head>
<title>Editing</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style_Editing.css">

</head>

<?php

$tpc = "";
$cnt = "";
$cat = "";
$mem = "";
$com = "";

if(isset($form))
{
	$tpc = str_replace(array("<i>","</i>"),array("[i]","[/i]"),$form->topic);
	$cnt = str_replace(array("<a href='","'ca>","</a>","<b>","</b>","<i>","</i>","<img src='","'cimg>"),array('[link="','"/link]',"[/link]","[b]","[/b]","[i]","[/i]",'[img="','"/img]'),$form->content);
	$cat = $form->category;
	$mem = $form->member;
	$com = $form->comment;
}
?>

<body onLoad="initialize()">

<div class="heading">
Editing Section
<hr>
</div>

<div class="area1">
	<textarea class="topic" rows="1" onBlur="saveTopic()"><?php echo $tpc; ?></textarea>
	<textarea class="content" rows="40" onBlur="saveContent()"><?php echo $cnt; ?></textarea>
</div>
<div class="area2">
	<select class="dropdowncat" name="categoory" onchange="saveCategory()">
	<?php
	foreach($allCat as $acat)
	{
	?>
	<option value="<?php echo $acat->id ?>" <?php if($acat->id == $cat) { echo 'selected="selected"'; } ?>>Category: <?php echo $acat->name ?></option>
	<?php
	}
	?>
	</select>
	<div class="clear"></div>
	<select class="dropdownL" name="member" onblur="saveMember()">
	<option value="1" <?php if($mem == 1) { echo 'selected="selected"'; } ?>>Member view only</option>
	<option value="0" <?php if($mem == 0) { echo 'selected="selected"'; } ?>>Public view</option>
	</select>
	<select class="dropdownR" name="comment" onblur="saveComment()">
	<option value="1" <?php if($com == 1) { echo 'selected="selected"'; } ?>>Allow commenting</option>
	<option value="0" <?php if($com == 0) { echo 'selected="selected"'; } ?>>No commenting</option>
	</select>
	<div class="clear"></div>
	<button class="btnBack" onclick="window.location='./?controller=posts&action=index';">Back</button>
	<form action="?controller=posts&action=preview&id=<?php echo $_GET['id']; ?>" method="POST">
	<input type="submit" class="btnPreview" value="Preview">
	<input type="hidden" name="topic" id="tpc">
	<input type="hidden" name="content" id="cnt">
	<input type="hidden" name="category" id="cat">
	<input type="hidden" name="member" id="mem">
	<input type="hidden" name="comment" id="com">
	</form>
</div>
<div class="clear"></div>
</body>

<script>
function initialize()
{
	document.getElementById("tpc").value = document.getElementsByClassName("topic")[0].value;
	document.getElementById("cnt").value = document.getElementsByClassName("content")[0].value;
	document.getElementById("mem").value = document.getElementsByClassName("dropdownL")[0].value;
	document.getElementById("com").value = document.getElementsByClassName("dropdownR")[0].value;
	document.getElementById("cat").value = document.getElementsByClassName("dropdowncat")[0].value;
}

function saveTopic()
{
	document.getElementById("tpc").value = document.getElementsByClassName("topic")[0].value;
}

function saveContent()
{
	document.getElementById("cnt").value = document.getElementsByClassName("content")[0].value;
}

function saveMember()
{
	document.getElementById("mbm").value = document.getElementsByClassName("dropdownL")[0].value;
}

function saveComment()
{
	document.getElementById("com").value = document.getElementsByClassName("dropdownR")[0].value;
}

function saveCategory()
{
	document.getElementById("cat").value = document.getElementsByClassName("dropdowncat")[0].value;
}

</script>

</html>