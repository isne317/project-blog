<?php

function error($id)
{
	header('location: ./?controller=pages&action=register&err=' . $id . '&username=' . $_POST['username'] . '&email=' . $_POST['email']);
}

if($_POST['password'] != $_POST['confpassword'])
{
	error(1);
}
elseif(Session::checkUsernameExists($_POST['username']))
{
	error(2);
}
elseif(Session::checkEmailExists($_POST['email']))
{
	error(3);
}
else
{
	Session::register($_POST['username'], $_POST['password'], $_POST['email']);
	Session::login($_POST['username'], $_POST['password']);
	header('location: ./');
}

?>