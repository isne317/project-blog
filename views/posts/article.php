<html>

<head>
<title><?php echo Post::getAuthorName($post->author_id) . ' - ' . $post->topic; ?></title>
<link rel="stylesheet" type="text/css" href="styles/style_Article.css">
<script type="text/javascript" src="jscript/article.js"></script>
</head>

<body>
<?php if($post->member == 0 || Session::check_login()){ ?>
<div class="articleArea">

<div class="topic">
<?php echo $post->topic; ?>
</div>
<div class="content">
<?php echo nl2br($post->content); ?>
</div>
<div class="clear"></div>
<div class="footnote">
<?php

$author = "{<a href='?controller=posts&action=myarticles&id=" . $post->author_id . "'>" . Post::getAuthorName($post->author_id) . "</a>}";
$category = "<a href='?controller=posts&action=catview&view=" . $post->category . "'>" . Post::getCatName($post->category) . "</a>";

echo "Posted/Last update: " . $post->timestamp;
echo " | Written by: " . $author;
echo " | Posted in: " . $category;

?>
</div>

<hr>

</div>

<div class="commentArea">

<div class="like"><b><?php echo $post->likes ?></b> people like this. <?php if(Session::check_login()) { ?><a href="?controller=posts&action=likepost&view=<?php echo $post->id ?>">Like</a><?php }?></div><br>
<div class="commentHead">Comments</div><br>
<?php

foreach($comments as $comment)
{
	$additional = "";
	$additional2 = "";
	if($comment->com_id != "-1")
	{
		$additional = "<a href='?controller=posts&action=myarticles&id=" . $comment->com_id . "'>";
		$additional2 = "</a>";
	}
	$likebutt = "";
	if(Session::check_login())
	{
		$likebutt = "<a href=?controller=posts&action=likecomment&view=" . $post->id . "&id=" . $comment->id . ">[Like]</a>";
	}
	echo "<div class='commenter'>" . $additional . $comment->name . $additional2 . "</div>";
	echo "<div class='commentInfo'>[" . $comment->timestamp . "] | <b>" . $comment->likes . "</b> likes " . $likebutt . "</div><div class='clear'></div><br>";
	echo nl2br("<div class='commentContent'>" . $comment->comment . "</div><hr class='line'><div class='clear'></div>");

}
if($post->comment == 1)
{
?>
</div>
<div class="clear"></div>
<br>
<form method="POST" action="?controller=posts&action=addcomment&view=<?php echo $post->id ?>">
<input type="text" class="commentField_nme" name="newName" rows="1" onblur="saveName()"  placeholder="name - 3-20 chars or blank" <?php if(Session::check_login()) { echo 'readonly value="' . $_SESSION['username'] . '"' ;}?>>
<input type="text" class="commentField_cmt" name="newComment"rows="1" onblur="saveComment()" placeholder="256 characters limit">
<input type="submit" class="btnComment" value="Comment">

</form>
<div class="clear"></div>

<?php
}
else
{
	echo "<br><span class='commentoff'>The author has turned off commenting for this article.</span>";
}
}
else
{
	echo "You aren't allowed to view this article.";
}
?>

</body>

</html>