<html>

<head>
<title>Categories</title>
<link rel="stylesheet" type="text/css" href="styles/style_category.css">

</head>

<body>

<?php

echo "<div class='head'>Choose category</div><br>";

foreach($categories as $category)
{
	echo "<div class='items'><a href='?controller=posts&action=catview&view=" . $category->id . "'>[" . $category->name . "]</a></div>";
}
?>
<br>
<form action="?controller=posts&action=addcat" method="POST">
<input type="text" class="addCategory" rows="1" placeholder="Max 20 characters">
<input type="submit" class="btnAddCat" value="Add">
<input type="hidden" id="name" name="name">
</form>
<div class="clear"></div>

</body>

</html> 