<html>

<head>
<title>Articles</title>
<link rel="stylesheet" type="text/css" href="styles/style_mainPage.css">

</head>

<body>

<?php
foreach($posts as $post)
{
	$topic = "<a href='?controller=posts&action=article&view=" . $post->id . "'>" . $post->topic . "</a>";
	$edit = "<a href='?controller=posts&action=edit&id=" . $post->id . "'>Edit</a>";
	$remove = "<a href='?controller=posts&action=remove&id=" . $post->id . "'>Remove</a>";
	$author = "{<a href='?controller=posts&action=myarticles&id=" . $post->author_id . "'>" . Post::getAuthorName($post->author_id) . "</a>}";
	$minicontent = str_replace(array("&#34","&#39","&#60","&#62","<img src='","'cimg>"),array('"',"'","<",">",'[img=&#34','&#34/img]'),substr($post->content,0,200));
	if(strlen($post->content) > 200)
	{
		$minicontent = $minicontent . "...";
	}
	$category = "<a href='?controller=posts&action=catview&view=" . $post->category . "'>" . Post::getCatName($post->category) . "</a>";
	if(Session::check_login() || $post->member == 0)
	{
		echo "<div class='items'>";
		echo $topic;
		echo "</div><div class='subitems'>";
		echo "Posted/Last update: " . $post->timestamp;
		echo " | Written by: " . $author;
		echo " | Posted in: " . $category;
		echo " ";
		if(Session::check_login())
		{
			if($_SESSION['id'] == $post->author_id)
			{
				echo "<button class='btnEdit'>" . $edit . "</button> <button class='btnRemove'>" . $remove . "</button>";
			}
		}
		echo "</div><br>";
		echo "<div class='previewContent'>" . $minicontent . "</div>";
		echo "<br><hr class='line'><div class='clear'></div><div class='clear'></div>";
	}
}
?>

</body>

</html> 