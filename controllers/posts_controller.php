<?php

class PostsController
{
	public function edit()
	{
		if(!isset($_GET['id']))
		{
			return call('pages', 'error');
		}
		if($_GET['id'] != "new")
		{
			$form = Post::get($_GET['id']);
		}
		$allCat = Category::all();
		require_once('views/posts/edit.php');
	}
	
	public function preview()
	{
		$cat = Category::get($_POST['category']);
		require_once('views/posts/preview.php');
	}
	
	public function index()
	{
		$posts = Post::all();
		require_once('views/posts/index.php');
	}
	
	public function article()
	{
		if(!isset($_GET['view']))
		{
			return call('pages', 'error');
		}
		$post = Post::get($_GET['view']);
		$comments = Comment::getFromBlogId($_GET['view']);
		require_once('views/posts/article.php');
	}
	
	public function process()
	{
		if(!isset($_GET['id']))
		{
			return call('pages', 'error');
		}
		
		if($_GET['id'] == "new")
		{
			$id = Post::add($_POST['au'], $_POST['tpc'], $_POST['cnt'], $_POST['cat'], $_POST['mem'], $_POST['com']);
		}
		else
		{
			$id = $_GET['id'];
			Post::update($id, $_POST['tpc'], $_POST['cnt'], $_POST['cat'], $_POST['mem'], $_POST['com']);
		}
		require_once('views/posts/process.php');
	}
	
	public function remove()
	{
		if(!isset($_GET['id']))
		{
			return call('pages', 'error');
		}
		
		Post::remove($_GET['id']);
		require_once('views/posts/process.php');
	}
	
	public function likepost()
	{
		if(!isset($_GET['view']))
		{
			return call('pages', 'error');
		}
		Post::like($_GET['view']);
		require_once('views/posts/self_redirect.php');
	}
	
	public function likecomment()
	{
		if(!isset($_GET['id']))
		{
			return call('pages', 'error');
		}
		Comment::like($_GET['id']);
		require_once('views/posts/self_redirect.php');
	}
	
	public function addcomment()
	{
		$pname = "/^((?=[^ ]).{3,20}|)$/";
		$pcomment = "/^(?=[^ ]).{1,256}$/m";
		$name = str_replace(array('"',"'","<",">","[b]","[/b]"),array("&#34","&#39","&#60","&#62","<b>","</b>"),$_POST['newName']);
		$comment = str_replace(array('"',"'","<",">","[b]","[/b]"),array("&#34","&#39","&#60","&#62","<b>","</b>"),$_POST['newComment']);
		if(preg_match($pname, $name) && preg_match($pcomment, $comment))
		{
			if($name == "")
			{
				$name = "Anonymous";
			}
			if(!Session::check_login())
			{
				$name = $name . "(Guest)";
				$id = -1;
			}
			else
			{
				$id = $_SESSION['id'];
			}
			Comment::add($id, $_GET['view'], $name, $comment);
		}
		require_once('views/posts/self_redirect.php');
	}
	
	public function categories()
	{
		$categories = Category::all();
		require_once('views/posts/categories.php');
	}
	
	public function catview()
	{
		$list = Category::getPostsInCategory($_GET['view']);
		require_once('views/posts/categorylist.php');
	}
	
	public function addcat()
	{
		Category::add($_POST['name']);
		header('location: ./?controller=posts&action=categories');
	}
	
	public function register()
	{
		require_once('views/posts/register.php');
	}
	
	public function myarticles()
	{
		if(!isset($_GET['id']))
		{
			return call('pages', 'error');
		}
		$list = Post::getByAuthorId($_GET['id']);
		require_once('views/posts/myarticles.php');
	}
}

?>