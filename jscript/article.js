function saveComment()
{
	val = document.getElementsByClassName("commentField_cmt")[0].value;
	if(val.search(/^(?=[^ ]).{1,256}$/m) != -1)
	{
		document.getElementsByClassName("commentField_cmt")[0].style.border = "";
	}
	else
	{
		document.getElementsByClassName("commentField_cmt")[0].style.border = "1px solid red";
	}
}

function saveName()
{
	var val = document.getElementsByClassName("commentField_nme")[0].value;
	if(val.search(/^((?=[^ ]).{3,20}|)$/i) != -1)
	{
		document.getElementsByClassName("commentField_nme")[0].style.border = "";
	}
	else
	{
		document.getElementsByClassName("commentField_nme")[0].style.border = "1px solid red";
	}
}