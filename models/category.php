<?php

class Category
{
	public $id;
	public $name;
	
	public function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name;
	}
	
	public static function all()
	{
		$list = [];
		$db = DB::getInstance();
		$req = $db->query('SELECT * FROM categories');
		
		foreach($req->fetchAll() as $category)
		{
			$list[] = new Category($category['id'], $category['category']);
		}
		
		return $list;
	}
	
	public static function get($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM categories WHERE id = :id');
		$req->execute(array('id' => $id));
		$cat = $req->fetch();
		
		return new Category($cat['id'], $cat['category']);
	}
	
	public static function getPostsInCategory($category)
	{
		require_once('models/post.php');
		$list = [];
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM contents WHERE category = :cat');
		$req->execute(array('cat' => $category));
		foreach($req->fetchAll() as $post)
		{
			$list[] = new Post($post['id'], $post['author_id'], $post['topic'], $post['content'], $post['timestamp'], $post['likes'], $post['category'], $post['member'], $post['comment']);
		}
		return $list;
	}
	
	public function add($category)
	{
		$db = DB::getInstance();
		$req = $db->prepare('INSERT INTO categories(id, category) VALUES (NULL, :cat)');
		$req->execute(array('cat' => $category));
	}
	
	public function update($id, $category)
	{
		$db = DB::getInstance();
		$req = $db->prepare('UPDATE categories SET category = :cat WHERE categories.id = :id');
		$req->execute(array('cat' => $category, 'id' => $id));
	}
}

?>