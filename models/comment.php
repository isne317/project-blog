<?php

class Comment
{
	private $blog_id;
	public $com_id;
	public $id;
	public $name;
	public $comment;
	public $timestamp;
	public $likes;
	
	public function __construct($id, $com_id, $blog_id, $name, $comment, $timestamp, $likes)
	{
		$this->com_id = $com_id;
		$this->blog_id = $blog_id;
		$this->id = $id;
		$this->name = $name;
		$this->comment = $comment;
		$this->timestamp = $timestamp;
		$this->likes = $likes;
	}
	
	public static function all()
	{
		$list = [];
		$db = DB::getInstance();
		$req = $db->query('SELECT * FROM comments');
		
		foreach($req->fetchAll() as $comment)
		{
			$list[] = new Comment($comment['id'], $comment['com_id'], $comment['blog_id'], $comment['name'], $comment['comment'], $comment['timestamp'], $comment['likes']);
		}
		
		return $list;
	}
	
	public static function getFromBlogId($id)
	{
		$list = [];
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM comments WHERE blog_id = :id');
		$req->execute(array('id' => $id));
		
		foreach($req->fetchAll() as $comment)
		{
			$list[] = new Comment($comment['id'], $comment['com_id'], $comment['blog_id'], $comment['name'], $comment['comment'], $comment['timestamp'], $comment['likes']);
		}
		
		return $list;
	}
	
	public static function get($id)
	{
		$db = DB::getInstance();
		$id = intval($id);
		$req = $db->prepare('SELECT * FROM comments WHERE id = :id');
		$req->execute(array('id' => $id));
		$comment = $req->fetch();
		
		return new Comment($comment['id'], $comment['com_id'], $comment['blog_id'], $comment['name'], $comment['comment'], $comment['timestamp'], $comment['likes']);
	}
	
	public function add($com_id, $blog_id, $name, $comment)
	{
		$db = DB::getInstance();
		$blog_id = intval($blog_id);
		$req = $db->prepare('INSERT INTO comments (id, com_id, blog_id, name, comment, timestamp, likes) VALUES (NULL, :cid, :blg, :nme, :cmt, CURRENT_TIMESTAMP, 0)');
		$req->execute(array('cid' => $com_id, 'blg' => $blog_id, 'nme' => $name, 'cmt' => $comment));
	}
	
	public function like($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM comments WHERE id = :id');
		$req->execute(array('id' => $id));
		$post = $req->fetch();
		$temp = intval($post['likes']);
		$temp++;
		$req2 = $db->prepare('UPDATE comments SET likes = :lks WHERE comments.id = :id');
		$req2->execute(array('lks' => $temp, 'id' => $id));
	}
}

?>