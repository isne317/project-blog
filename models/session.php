<?php

session_start();

class Session
{
	
	public function check_login()
	{
		return isset($_SESSION['id']) && isset($_SESSION['username']);
	}
	
	public function login($username, $password)
	{
		if(!Session::check_login())
		{
			$db = DB::getInstance();
			$req = $db->prepare('SELECT id,username,password FROM users WHERE username = :user LIMIT 1');
			$req->execute(array('user' => $username));
			$user = $req->fetch();
			$db_password = $user['password'];
			if($password == $db_password)
			{
				$_SESSION['id'] = $user['id'];
				$_SESSION['username'] = $user['username'];
			}
		}
	}
	
	public function logout()
	{
		session_destroy();
	}
	
	public function checkUsernameExists($username)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT id FROM users WHERE username = :user LIMIT 1');
		$req->execute(array('user' => $username));
		$user = $req->fetch();
		if($user['id'] != "")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function checkEmailExists($email)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT id FROM users WHERE email = :eml LIMIT 1');
		$req->execute(array('eml' => $email));
		$user = $req->fetch();
		if($user['id'] != "")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function register($username, $password, $email)
	{
		$db = DB::getInstance();
		$req = $db->prepare('INSERT INTO users(id,username,password,email) VALUES (NULL, :usr, :pas, :eml)');
		$req->execute(array('usr' => $username, 'pas' => $password, 'eml' => $email));
	}
}

if(isset($_GET['session']))
{
	if($_GET['session'] == "out")
	{
		Session::logout();
		header('location: ./');
	}
	elseif($_GET['session'] == "in")
	{
		Session::login($_POST['username'], $_POST['password']);
		header('location: ./');
	}
}
$display = "";
if(Session::check_login())
{
	$display = "Logged in as " . $_SESSION['username'] . " <a href='?controller=" . $controller . "&action=" . $action . "&session=out'>[Logout]</a>";
}
else
{
	$display = "Guest view";
}

?>