<?php

class Post
{
	public $id;
	public $author_id;
	public $topic;
	public $content;
	public $timestamp;
	public $likes;
	public $category;
	public $member;
	public $comment;
	
	public function __construct($id, $author_id, $topic, $content, $timestamp, $likes, $category, $member, $comment)
	{
		$this->id = $id;
		$this->author_id = $author_id;
		$this->topic = $topic;
		$this->content = $content;
		$this->timestamp = $timestamp;
		$this->likes = $likes;
		$this->category = $category;
		$this->member = $member;
		$this->comment = $comment;
	}
	
	public static function all()
	{
		$list = [];
		$db = DB::getInstance();
		$req = $db->query('SELECT * FROM contents');
		
		foreach($req->fetchAll() as $post)
		{
			$list[] = new Post($post['id'], $post['author_id'], $post['topic'], $post['content'], $post['timestamp'], $post['likes'], $post['category'], $post['member'], $post['comment']);
		}
		
		return $list;
	}
	
	public static function get($id)
	{
		$db = DB::getInstance();
		$id = intval($id);
		$req = $db->prepare('SELECT * FROM contents WHERE id = :id');
		$req->execute(array('id' => $id));
		$post = $req->fetch();
		
		return new Post($post['id'], $post['author_id'], $post['topic'], $post['content'], $post['timestamp'], $post['likes'], $post['category'], $post['member'], $post['comment']);
	}
	
	public static function getByAuthorId($id)
	{
		$list = [];
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM contents WHERE author_id = :id');
		$req->execute(array('id' => $id));
		
		foreach($req->fetchAll() as $post)
		{
			$list[] = new Post($post['id'], $post['author_id'], $post['topic'], $post['content'], $post['timestamp'], $post['likes'], $post['category'], $post['member'], $post['comment']);
		}
		
		return $list;
	}
	
	public function add($author_id, $topic, $content, $category, $member, $comment)
	{
		$db = DB::getInstance();
		$req = $db->prepare('INSERT INTO contents(id,author_id,topic,content,timestamp,likes,category,member,comment) VALUES (NULL, :au, :tpc, :cnt, CURRENT_TIMESTAMP, 0, :cat, :mem, :com)');
		$req->execute(array('au' => $author_id, 'tpc' => $topic, 'cnt' => $content, 'cat' => $category, 'mem' => $member, 'com' => $comment));
		$req2 = $db->query('SELECT * FROM contents ORDER BY id DESC LIMIT 1');
		$post = $req2->fetch();
		
		return $post['id'];
	}
	
	public function update($id, $topic, $content, $category, $member, $comment)
	{
		$db = DB::getInstance();
		$req = $db->prepare('UPDATE contents SET topic = :tpc, content = :cnt, category = :cat, member = :mem, comment = :com, timestamp = CURRENT_TIMESTAMP WHERE contents.id = :id');
		$req->execute(array('tpc' => $topic, 'cnt' => $content, 'cat' => $category, 'mem' => $member, 'com' => $comment, 'id' => $id));
	}
	
	public function remove($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('DELETE FROM contents WHERE id = :id');
		$req->execute(array('id' => $id));
		$req2 = $db->prepare('DELETE FROM comments WHERE blog_id = :id');
		$req2->execute(array('id' => $id));
	}
	
	public function like($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM contents WHERE id = :id');
		$req->execute(array('id' => $id));
		$post = $req->fetch();
		$temp = intval($post['likes']);
		$temp++;
		$req2 = $db->prepare('UPDATE contents SET likes = :lks WHERE contents.id = :id');
		$req2->execute(array('lks' => $temp, 'id' => $id));
	}
	
	public function getCatName($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM categories WHERE id = :id');
		$req->execute(array('id' => $id));
		$cat = $req->fetch();
		
		return $cat['category'];
	}
	
	public function getAuthorName($id)
	{
		$db = DB::getInstance();
		$req = $db->prepare('SELECT * FROM users WHERE id = :id');
		$req->execute(array('id' => $id));
		$user = $req->fetch();
		
		return $user['username'];
	}
}

?>