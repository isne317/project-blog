<?php

$header = "";

function call($controller, $action)
{
	require_once('controllers/' . $controller . '_controller.php');
	
	switch($controller)
	{
		case 'pages': $controller = new PagesController(); break;
		case 'posts': require_once('models/post.php');
			require_once('models/comment.php');
			require_once('models/category.php');
			$controller = new PostsController(); break;
	}
	
	$controller->{ $action }();
}

$controllers = array('pages' => ['home', 'error', 'register'], 'posts' => ['edit', 'preview', 'index', 'categories', 'article', 'process', 'remove', 'likepost', 'likecomment', 'addcomment', 'catview', 'addcat', 'register', 'myarticles']);

if(array_key_exists($controller, $controllers))
{
	if(in_array($action, $controllers[$controller]))
	{
		call($controller, $action);
	}
	else
	{
		call('pages', 'error');
	}
}
else
{
	call('pages', 'error');
}

?>